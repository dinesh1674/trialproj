const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const placesSchema=new Schema({
    placename:{
        type: String,
        required: true,
        unique: true,
        minlength: 3
    },
    description:{
        type: String,
        required:true,
        minlength:20,
        maxlength:100

    },
    background:{
        type: String,
        required:true,
        minlength:10,
        maxlength:100
    },
    city:{
        type:String,
        required:true,
    },
    state:{
        type:String,
        required:true,
    },
    thingstodo:{
        type:Array,
        "default" : []
    },
    category:{
        type:Array,
        "default" : []
    },
    keywords:{
        type:Array,
        "default" : []
    },
    totalvisits:{
        type: Number,
        required:true,
        default:0
    },
    placephotos:{
        type:[{
        Image1 : String,
        Image2 : String,
        Image3 : String,
        Image4 : String,
        Image5 : String,
        }],
        "default":["","","","",""]
    },
    },{
        timestamps: true,
    }
);

const Place= mongoose.model('Place',placesSchema);

module.exports= Place;