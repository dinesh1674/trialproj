const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema=new Schema({
    username:{
        type: String,
        required: true,
        unique: true,
        minlength: 3
    },
    name:{
        type: String,
        required: true,
        minlength: 3
    },
    mobilenumber:{
        type: String,
        required:true,
        minlength:10,
        maxlength:10

    },
    age:{
        type: Number,
        required:true
    },
    gender:{
        type: String,
        required:true
    },
    emailid:{
        type:String,
        required:true,
        unique: true
    },
    password:{
        type:String,
        required:true,
        minlength:6
    },
    maritalstatus:{
        type:Boolean,
        required:true,
    },
    city:{
        type:String,
        required:true,
    },
    state:{
        type:String,
        required:true,
    },
    mostfrequent:{
        type:[{
        hillstation : Number,
        sightseeing : Number,
        adventure : Number,
        couple : Number,
        monument : Number,
        beach : Number,
        wildlife : Number,
        }],
        "default":[0,0,0,0,0,0,0]
    },
    },{
        timestamps: true,
    }
);

const User= mongoose.model('User',userSchema);

module.exports= User;