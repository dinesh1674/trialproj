const router = require('express').Router();
let Place= require('../models/place.model');

router.route('/').get((req,res)=>{
    Place.find()
        .then(places => res.json(places))
        .catch(err => res.status(400).json('Error: '+ err));
});

router.route('/add').post((req,res)=>{
    const placename= req.body.placename;
    const description= req.body.description;
    const background= req.body.background;
    const city= req.body.city;
    const state= req.body.state;
    const thingstodo= Array(req.body.thingstodo);
    const category= Array(req.body.category);
    const keywords= Array(req.body.keywords);
    const totalvisits= req.body.totalvisits;
    const placephotos= Array(req.body.placephotos);
    const date= Date.parse(req.body.date);

    const newPlaces= new Place({
        placename,
        description,
        background,
        city,
        state,
        thingstodo,
        category,
        keywords,
        totalvisits,
        placephotos,
        date
    });


    newPlaces.save()
        .then(()=>res.json('Places added!'))
        .catch(err=>res.status(400).json('Error: '+ err));
});

router.route('/:id').get((req, res) => {
    Place.findById(req.params.id)
      .then(places => res.json(places))
      .catch(err => res.status(400).json('Error: ' + err));
  });
  
  router.route('/:id').delete((req, res) => {
    Place.findByIdAndDelete(req.params.id)
      .then(() => res.json('Places deleted.'))
      .catch(err => res.status(400).json('Error: ' + err));
  });
  
  router.route('/update/:id').post((req, res) => {
    Place.findById(req.params.id)
      .then(places => {
        places.placename= req.body.placename;
        places.description= req.body.description;
        places.background= req.body.background;
        places.city= req.body.city;
        places.state= req.body.state;
        places.thingstodo= Array(req.body.thingstodo);
        places.category= Array(req.body.category);
        places.keywords= Array(req.body.keywords);
        places.totalvisits= req.body.totalvisits;
        places.placephotos= Array(req.body.placephotos);
        places.date= Date.parse(req.body.date);
  
        places.save()
          .then(() => res.json('Places updated!'))
          .catch(err => res.status(400).json('Error: ' + err));
      })
      .catch(err => res.status(400).json('Error: ' + err));
  });

module.exports = router;