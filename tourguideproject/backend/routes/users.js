const router = require('express').Router();
let User= require('../models/user.model');

router.route('/').get((req,res)=>{
    User.find()
        .then(users => res.json(users))
        .catch(err => res.status(400).json('Error: '+ err));
});

router.route('/add').post((req,res)=>{
    const username= req.body.username;
    const name= req.body.name;
    const mobilenumber= req.body.mobilenumber;
    const age= Number(req.body.age);
    const gender= req.body.gender;
    const emailid= req.body.emailid;
    const password= req.body.password;
    const maritalstatus= Boolean(req.body.maritalstatus);
    const city= req.body.city;
    const state= req.body.state;
    const mostfrequent= Array(req.body.mostfrequent);
    const date= Date.parse(req.body.date);

    const newUser= new User({
        username,
        name,
        mobilenumber,
        age,
        gender,
        emailid,
        password,
        maritalstatus,
        city,
        state,
        mostfrequent,
        date
    });


    newUser.save()
        .then(()=>res.json('User added!'))
        .catch(err=>res.status(400).json('Error: '+ err));
});

router.route('/:id').get((req, res) => {
    User.findById(req.params.id)
      .then(users => res.json(users))
      .catch(err => res.status(400).json('Error: ' + err));
  });
  
  router.route('/:id').delete((req, res) => {
    User.findByIdAndDelete(req.params.id)
      .then(() => res.json('User deleted.'))
      .catch(err => res.status(400).json('Error: ' + err));
  });
  
  router.route('/update/:id').post((req, res) => {
    User.findById(req.params.id)
      .then(users => {
        users.username= req.body.username;
        users.name= req.body.name;
        users.mobilenumber= req.body.mobilenumber;
        users.age= Number(req.body.age);
        users.gender= req.body.gender;
        users.emailid= req.body.emailid;
        users.password= req.body.password;
        users.maritalstatus= Boolean(req.body.maritalstatus);
        users.city= req.body.city;
        users.state= req.body.state;
        users.mostfrequent= Array(req.body.mostfrequent);
        users.date= Date.parse(req.body.date);
  
        users.save()
          .then(() => res.json('User updated!'))
          .catch(err => res.status(400).json('Error: ' + err));
      })
      .catch(err => res.status(400).json('Error: ' + err));
  });

module.exports = router;